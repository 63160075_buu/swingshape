/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.shapeproject2;

/**
 *
 * @author ACER
 */
public abstract class Shape {
    private String shapename;

    public Shape(String shapename) {
        this.shapename = shapename;
    }

    public String getShapename() {
        return shapename;
    }
    
    public abstract double calArea();
    public abstract double calPerimeter();
}
